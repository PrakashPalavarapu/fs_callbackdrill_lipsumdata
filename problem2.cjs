const fs = require("fs");

function lipsumDataFiles() {
  fs.writeFile("./fileNames.txt", "", "utf-8", (err) => {
    if (err) {
      throw Error("failed to clear data in fileNames.txt");
    }
  });
  fs.readFile("./lipsum_1.txt", "utf-8", (err, data) => {
    if (err) {
      throw new Error("reading lipsum_1.txt failed");
    } else {
      let uppperData = data.toUpperCase();
      console.log("reading lipsum_1.txt successfully");
      // creating upperLipsum
      fs.writeFile("./upperLipsum.txt", uppperData, "utf-8", (err) => {
        if (err) {
          throw new Error("failed to create upperLipsum.txt");
        } else {
          console.log("created upperLipsum.txt");
          // adding upperLipsum to fileNames.txt
          fs.appendFile("./fileNames.txt", `./upperLipsum.txt`, (err) => {
            if (err) {
              throw new Error("failed to add upperLipsum.txt to file names");
            } else {
              console.log("upperLipsum added to fileNames");
              let lowerSentences = uppperData.toLowerCase().split(".");
              // creating lowerSentencesList
              fs.writeFile(
                "./lowerSentencesList.txt",
                JSON.stringify(lowerSentences),
                "utf-8",
                (err) => {
                  if (err) {
                    throw new Error("failed to create lowerSentencesList.txt");
                  } else {
                    console.log("created lowerSentencesList.txt");
                    // adding lowerSentencesList to fileNames
                    fs.appendFile(
                      "./fileNames.txt",
                      "./lowerSentencesList.txt",
                      (err) => {
                        if (err) {
                          throw new Error(
                            "failed to add ./lowerSentencesList.txt"
                          );
                        } else {
                          console.log(
                            "added ./lowerSentencesList.txt to filenames"
                          );
                          let sortedLowerSentences = lowerSentences.sort();
                          // creating sortedLowerSentences
                          fs.writeFile(
                            "./sortedLowerSentences.txt",
                            JSON.stringify(sortedLowerSentences),
                            "utf-8",
                            (err) => {
                              if (err) {
                                throw new Error(
                                  "failed to create sortedLowerSentences.txt"
                                );
                              } else {
                                console.log("created sortedLowerSentences.txt");
                                // adding sortedLowerSentences to fileNames
                                fs.appendFile(
                                  "./fileNames.txt",
                                  "./sortedLowerSentences.txt",
                                  (err) => {
                                    if (err) {
                                      throw new Error(
                                        "failed to add ./sortedLowerSentences.txt to filenames"
                                      );
                                    } else {
                                      console.log(
                                        "added ./sortedLowerSentences.txt to filenames"
                                      );
                                      fs.readFile(
                                        "./fileNames.txt",
                                        "utf-8",
                                        (err, files) => {
                                          console.log("here");
                                          if (err) {
                                            throw new Error(
                                              "failed to read fileNames.txt"
                                            );
                                          } else {
                                            let filesList = files.split("./");
                                            // console.log(filesList);
                                            for (let file of filesList) {
                                              if (file != "") {
                                                fs.unlink(file, (err) => {
                                                  if (err) {
                                                    throw new Error(
                                                      `failed to delete ${file}`
                                                    );
                                                  } else {
                                                    console.log(
                                                      `deleted ${file}`
                                                    );
                                                  }
                                                });
                                              }
                                            }
                                          }
                                        }
                                      );
                                    }
                                  }
                                );
                              }
                            }
                          );
                        }
                      }
                    );
                  }
                }
              );
            }
          });
        }
      });
    }
  });
}
module.exports = lipsumDataFiles;
