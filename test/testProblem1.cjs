const fs = require("fs");
const createRandomJSONfiles = require("../problem1.cjs")
// callback for file creation
let fileCreationCallback = (filename) => {
  fs.writeFile(
    `./JSONfiles/${filename}.json`,
    `This is ${filename}`,
    "utf-8",
    (err) => {
      if (err) {
        throw new Error(`json file ${filename} creation failed`);
      } else {
        console.log(`file ${filename} created`);
        fileDeletionCallback(filename);
      }
    }
  );
};

// callback for file deletion
let fileDeletionCallback = (filename) => {
  fs.unlink(`./JSONfiles/${filename}.json`, (err) => {
    if (err) {
      throw new Error(`file ${filename} failed to delete`);
    } else {
      console.log(`file ${filename} deleted`);
    }
  });
};

createRandomJSONfiles(fileCreationCallback);