const fs = require("fs");

// directory creation
function createRandomJSONfiles(creatorCallback) {
  fs.mkdir("./JSONfiles", { recursive: true }, (err) => {
    if (err) {
      throw new Error("Folder creation failed");
    } else {
      console.log("Json folder created");
      // creating random files
      for (let fileCount = 0; fileCount < 5; fileCount++) {
        creatorCallback(`json${fileCount}`);
      }
    }
  });
}

module.exports = createRandomJSONfiles;